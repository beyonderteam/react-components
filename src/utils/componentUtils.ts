import { Children, ReactChild, ReactElement, ReactNode } from 'react';

const findChildByType = (children: ReactNode,  type: Function): ReactChild | undefined => {
    return Children.toArray(children).find((child) => {
        const elementChild = child as ReactElement<object>;

        if (typeof(elementChild.type) === 'undefined') {
            return false;
        } else {
            return elementChild.type === type;
        }
    });
};

export {
    findChildByType
};