import * as React from 'react';
import * as PropTypes from 'prop-types';

import { ModalProps, ModalState, ModalBehavior } from '@beyonder/common/modal';
import { ModalHeader, ModalBody, ModalFooter } from './ModalParts';
import { findChildByType } from '../utils/componentUtils';

import './Modal.css';

export default class Modal extends React.Component<ModalProps, ModalState> implements ModalBehavior {

    static propTypes = {
        inClassname: PropTypes.string,
        outClassname: PropTypes.string,
        onClose: PropTypes.func
    };

    static defaultProps = {
        inClassname: '',
        outClassname: ''
    };

    private self: HTMLElement | null;

    constructor(props: ModalProps) {
        super(props);

        this.state = {
            isOpen: true
        };

        this.close = this.close.bind(this);
    }

    close(): void {
        const { outClassname, onClose } = this.props;

        const triggerOnClose = () => {
            if (typeof(onClose) !== 'undefined') {
                onClose();
            }

            if (this.self) {
                this.self.removeEventListener('animationend', triggerOnClose);
            }
        };

        this.setState({ isOpen: false }, () => {
            if (typeof(outClassname) === 'undefined' || outClassname.length === 0) {
                triggerOnClose();
            } else {
                if (this.self) {
                    this.self.addEventListener('animationend', triggerOnClose);
                }
            }
         });
    };

    render() {
        const { isOpen } = this.state;
        const { inClassname, outClassname, children } = this.props;

        return (
            <div
                className={"beyonder-modal " + (isOpen ? inClassname : outClassname)}
                ref={(element) => this.self = element}
            >
                <header>
                    <h5 className="modal-title">{findChildByType(children, ModalHeader)}</h5>
                    <button
                        type="button"
                        aria-label="Close the modal"
                        className="modal-close"
                        onClick={this.close}
                    >
                        ×
                    </button>
                </header>

                <main>{findChildByType(children, ModalBody)}</main>

                <footer>{findChildByType(children, ModalFooter)}</footer>
            </div>
        );
    }

}