import * as React from 'react';

abstract class ModalPart extends React.Component {

    render() {
        return this.props.children;
    }

}

class ModalHeader extends ModalPart {}
class ModalBody extends ModalPart {}
class ModalFooter extends ModalPart {}

export {
    ModalHeader,
    ModalBody,
    ModalFooter
};