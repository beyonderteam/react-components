import Modal from './Modal';
import { ModalHeader, ModalBody, ModalFooter } from './ModalParts';

export {
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter
}