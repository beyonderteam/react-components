# @beyonder/react-components
Implémentation des composants Beyonder pour la bibliothèque React.

## Phase de développement
Une fois ce projet cloné, il faudra faire un `npm install` puis un `npm link chemin_vers_le_beyonder_common`.

### Problèmes 
Afin que ce projet soit utilisable, contrairement au projet @beyonder/angular-components,
des actions temporaires doivent être faites en attendant que l'on trouve comment faire ça proprement.
Après avoir fait le `npm install` et le `npm link`, il faut utiliser la commande `tsc` (TypeScript Compilation) afin de générer les fichiers JS qui pourront être appelées depuis un autre projet React ou depuis notre catalogue de composants (projet Angular). Les composants transpilées sont générées dans le dossier build/dist, ce que je trouve pas très agréable à lire lors de l'import.

Exemple pour la modal :

Lorsque l'on fait un import d'un composant Angular depuis le catalogue, voici ce qui a besoin d'être écrit dans le app.module.ts :

import { ModalComponent } from '@beyonder/angular-components/modal';

```@NgModule({
  declarations: [
    ...
    ModalComponent
  ],
  ...
})```

Propre, simple, on l'importe et on le déclare dans le module.

Par contre, pour son homologue React, pour l'instant, on a : 
`import { Modal, ModalHeader, ModalBody, ModalFooter } from '@beyonder/react-components/build/dist/modal';`
On est obligé de passer par build/dist ... il faudra changer cela.